/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('testopenshif')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
