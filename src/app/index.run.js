(function() {
  'use strict';

  angular
    .module('testopenshif')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
