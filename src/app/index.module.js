(function() {
  'use strict';

  angular
    .module('testopenshif', ['ngAnimate', 'ngCookies', 'ngSanitize', 'ngResource', 'ui.router', 'ui.bootstrap', 'toastr']);

})();
